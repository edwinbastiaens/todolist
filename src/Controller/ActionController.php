<?php

namespace App\Controller;

use App\Entity\Action;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ActionController extends AbstractController
{
    /**
     * @Route("action/add", name="action_add", methods={"POST"})
     */
    public function add(Request $request)
    {
        $title = $request->get('title');

        if (null == $title) {
            return new Response('title missing',500);
        }

        $uid = $request->get('uid');
        if (null == $uid) {
            return new Response('uid missing', 500);
        }

        $this->getRepo()->add($uid, $title);

        return new Response('ok', 200);
    }

    /**
     * @Route("/action/done", name="action_done", methods={"POST"})
     */
    public function done(Request $request)
    {
        $actionId = $request->get('actionid');

        if (null == $actionId) {
            return new Response('actionid missing',500);
        }

        $this->getRepo()->done($actionId);

        return new Response('ok', 200);
    }

    /**
     * @return \App\Repository\ActionRepository
     */
    private function getRepo()
    {
        return $this->getDoctrine()->getRepository(Action::class);
    }
}
