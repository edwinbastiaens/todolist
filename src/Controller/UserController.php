<?php

namespace App\Controller;

use App\Entity\Action;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    public function home()
    {
        $user = $this->getUser();

        if (null === $user) {
            return $this->redirect($this->generateUrl('app_login'));
        }

        $actionRepo = $this->getDoctrine()
            ->getRepository(Action::class);

        $actionArr = $actionRepo->findByUserIdAndByStatus($user->getId(),'created');

        return $this->render(
            'user/home.html.twig',
            [
                'user' => $user,
                'actions' => $actionArr,
            ]
        );
    }
}
