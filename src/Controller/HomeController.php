<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    public function index()
    {
        $user = $this->getUser();

        if (null !== $user) {
            return $this->redirect($this->generateUrl('userhome'));
        }

        return $this->render(
            'home.html.twig',
            []
        );
    }
}
