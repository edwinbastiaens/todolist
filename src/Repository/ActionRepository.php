<?php

namespace App\Repository;

use App\Entity\Action;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Action|null find($id, $lockMode = null, $lockVersion = null)
 * @method Action|null findOneBy(array $criteria, array $orderBy = null)
 * @method Action[]    findAll()
 * @method Action[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Action::class);
    }

    /**
     * @param int $userId
     *
     * @return Action[] Returns an array of Action objects
     */
    public function findByUserId(int $userId)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.creator = :val')
            ->setParameter('val', $userId)
            ->orderBy('a.createdAt', 'ASC')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int    $userId
     * @param string $status
     *
     * @return mixed
     */
    public function findByUserIdAndByStatus(int $userId, $status)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.creator = :val')
            ->andWhere('a.status =:sts')
            ->setParameter('val', $userId)
            ->setParameter('sts', $status)
            ->orderBy('a.createdAt', 'ASC')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $actionId
     *
     * @return Action|null
     */
    public function findByActionId(int $actionId)
    {
        return $this->findOneBy(['id'=> $actionId]);
    }

    /**
     * @param int $userId
     * @param string $title
     */
    public function add(int $userId, $title)
    {
        $action = new Action();
        $action->setTitle($title);
        $action->setStatus('created');
        $action->setCreatedAt(new \DateTimeImmutable());
        $action->setCreator($userId);

        $this->save($action);
    }

    /**
     * @param Action $action
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function save($action)
    {
        $this->_em->persist($action);
        $this->_em->flush();
    }

    /**
     * @param int $actionId
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function done($actionId)
    {
        $action = $this->findByActionId($actionId);

        if (!$action) {
            return false;
        }

        $action->setStatus('done');
        $this->save($action);

        return true;
    }
}
