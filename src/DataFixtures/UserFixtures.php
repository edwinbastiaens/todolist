<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use function PHPSTORM_META\type;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $user = new User();

        $user->setFirstname('Zjef');
        $user->setLastname('Verpinten');
        $user->setEmail('john.doe@mail.com');
        $user->setRoles(['ROLE_ADMIN']);

        $pw = $this->passwordEncoder->encodePassword(
            $user,
            'the_new_password'
        );
        echo $pw;
        $user->setPassword(
            $pw
        );
        var_dump(compact('user'));
        $this->showBackTrace(4);
//        echo var_dump(debug_backtrace()[1]['function']);
//        echo var_dump(debug_backtrace()[1]['line']);
//        echo var_dump(debug_backtrace()[1]['file']);
//        echo var_dump(debug_backtrace()[2]['function']);
//        echo var_dump(debug_backtrace()[2]['line']);
//        echo var_dump(debug_backtrace()[2]['file']);
        $manager->flush();
    }

    private function showBackTrace($i)
    {
        if ($i < 0) { return;}
        echo $i .': ';
        if (isset(debug_backtrace()[$i]['file'])) {
            echo 'File: ' . debug_backtrace()[$i]['file'] . PHP_EOL;
        }
        if (isset(debug_backtrace()[$i]['function'])) {
            echo 'Function: ' . debug_backtrace()[$i]['function'];
        }
        if (isset(debug_backtrace()[$i]['line'])) {
            echo 'Line: ' . debug_backtrace()[$i]['line'] . PHP_EOL;
        }
        $this->showBackTrace($i-1);
    }
}
