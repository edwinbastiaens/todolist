<?php

namespace spec\App\Controller;

use App\Controller\ActionController;
use App\Entity\Action;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ActionControllerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ActionController::class);
    }

    function it_should_return_response_500_if_title_missing(Request $request)
    {
        $request->get('title')->willReturn(null);

        $this->add($request)->shouldBeLike(new Response('title missing',500));
    }

    function it_should_return_response_500_if_uid_missing(Request $request)
    {
        $request->get('title')->willReturn('wash dishes');
        $request->get('uid')->willReturn(null);

        $this->add($request)->shouldBeLike(new Response('uid missing',500));
    }
}
