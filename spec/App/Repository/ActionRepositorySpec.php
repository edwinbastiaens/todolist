<?php

namespace spec\App\Repository;

use App\Entity\Action;
use App\Repository\ActionRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ActionRepositorySpec extends ObjectBehavior
{
    function let(
        RegistryInterface $registry
    ) {
        $this->beConstructedWith($registry);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ActionRepository::class);
    }
}
