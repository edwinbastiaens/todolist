<?php
namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testRolesForAdmin()
    {
        $user = new User();
        $user->setRoles(['ROLE_ADMIN']);

        $this->assertTrue( in_array('ROLE_USER', $user->getRoles()));
        $this->assertTrue( in_array('ROLE_ADMIN', $user->getRoles()));
    }

    public function testRolesForUser()
    {
        $user = new User();

        $this->assertTrue( in_array('ROLE_USER', $user->getRoles()));
        $this->assertFalse( in_array('ROLE_ADMIN', $user->getRoles()));
    }
}
